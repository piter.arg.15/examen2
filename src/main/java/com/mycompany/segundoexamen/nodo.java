/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.segundoexamen;

/**
 *
 * @author Piter Argandoña Aramayo
 * CI  12717401
 */
class nodo<T> {
    public class nodo <T>{
    private T valor;
    private nodo<T> sigNod;
    
    public nodo(T valor, nodo<T> sigNod){
        this.valor=valor;
        this.sigNod=sigNod;
    }

    public T getValor() {
        return valor;
    }

    public void setValor(T valor) {
        this.valor = valor;
    }

    public nodo<T> getSigNod() {
        return sigNod;
    }

    public void setSigNod(nodo<T> sigNod) {
        this.sigNod = sigNod;
    }
    
     
    
}
}
